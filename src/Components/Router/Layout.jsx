import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import UseCallback from "../Class/useCallback/Index";
import UseMemo from "../Class/useMemo/UseMemo";

const Layout = () => {
  return (
    <div>
      <h1>React Router</h1>
      <a href="/usecallback">useCallback</a>
      <br />
      <a href="/usememo">useMemo</a>

      <BrowserRouter>
        <Routes>
          <Route path="/usecallback" element={<UseCallback />}>
            <Route index element={<UseCallback />} />
            <Route path="/usememo" element={<UseMemo />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default Layout;
