import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";

const Product = ({ img }) => {
  return (
    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
      <div className="product-card">
        <div className="position-relative w-100 overflow-hidden hover-zoom">
          <div className="button-wrapper pb-3 d-flex justify-content-center align-items-end position-absolute start-0 w-100 h-100 bg-transparent">
            <button
              type="button"
              data-bs-toggle="modal"
              data-bs-target="#productModal"
              className="button button-light btn btn-light rounded-pill px-4"
            >
              Quick View
            </button>
          </div>
          <img className="w-100 bg-image" src={img} alt="" />
        </div>
        <div className="card-body py-3 text-muted">
          <div className="d-flex justify-content-between align-items-center">
            <a
              className="product-title text-decoration-none"
              href="HTML/productDetail.html"
            >
              <p className="mb-0 text-dark">Black Sweater</p>
            </a>
            <span>
              <i className="fa-regular fa-heart"></i>
            </span>
          </div>
          <p className="card-text mb-0">$90.00</p>
        </div>
      </div>
    </div>
  );
};

export default Product;
