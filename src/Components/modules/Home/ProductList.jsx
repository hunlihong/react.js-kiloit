import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Product from "./Product";
import product01 from "../../../assets/product-01.jpg";
import product02 from "../../../assets/product-02.jpg";
import product03 from "../../../assets/product-03.jpg";
import product04 from "../../../assets/product-04.jpg";
import product05 from "../../../assets/product-05.jpg";
import product06 from "../../../assets/product-06.jpg";
import product07 from "../../../assets/product-07.jpg";
import product08 from "../../../assets/product-08.jpg";
import product09 from "../../../assets/product-09.jpg";
import product10 from "../../../assets/product-10.jpg";
import product11 from "../../../assets/product-11.jpg";
import product12 from "../../../assets/product-12.jpg";
import product13 from "../../../assets/product-13.jpg";
import product14 from "../../../assets/product-14.jpg";
import product15 from "../../../assets/product-15.jpg";
import product16 from "../../../assets/product-16.jpg";

const imgs = [
  product01,
  product02,
  product03,
  product04,
  product05,
  product06,
  product07,
  product08,
  product09,
  product10,
  product11,
  product12,
  product13,
  product14,
  product15,
  product16,
];

const ProductList = () => {
  return (
    <Container>
      <Row className="my-5">
        {imgs.map((img, index) => {
          return <Product key={index} img={img} />;
        })}
      </Row>
    </Container>
  );
};

export default ProductList;
