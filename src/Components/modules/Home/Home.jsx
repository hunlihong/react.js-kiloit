import React from "react";
import Hero from "./Hero";
import ProductList from "./ProductList";

const Home = () => {
  return (
    <div>
      <Hero />
      <ProductList />
    </div>
  );
};

export default Home;
