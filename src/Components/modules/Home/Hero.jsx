import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";
import slide01 from "../../../assets/slide-01.jpg";
import slide02 from "../../../assets/slide-02.jpg";
import slide03 from "../../../assets/slide-03.jpg";

const index = () => {
  return (
    <div
      className="carousel slide carousel-fade position-relative mb-5"
      id="carouselSlider"
      data-bs-ride="carousel"
    >
      <div className="carousel-inner">
        <div className="carousel-item active">
          <img src={slide01} className="d-block w-100" alt="" />
          <div className="container">
            <div className="carousel-caption text-dark text-start">
              <h3 data-aos="fade-down" data-aos-duration="2000">
                Women Collection 2018
              </h3>
              <h2
                data-aos="fade-up"
                data-aos-duration="2000"
                data-aos-delay="1000"
              >
                NEW SEASON
              </h2>
              <button className="button btn btn-primary rounded-pill px-4">
                <a
                  href="HTML/shop.html"
                  className="text-white text-decoration-none"
                >
                  Shop Now
                </a>
              </button>
            </div>
          </div>
        </div>
        <div className="carousel-item">
          <img src={slide02} className="d-block w-100" alt="" />
          <div className="container">
            <div className="carousel-caption text-dark text-start">
              <h3>Men New-Season</h3>
              <h2>JACKETS & COATS</h2>
              <button className="button btn btn-primary rounded-pill px-4">
                <a
                  href="HTML/shop.html"
                  className="text-white text-decoration-none"
                >
                  Shop Now
                </a>
              </button>
            </div>
          </div>
        </div>
        <div className="carousel-item">
          <img src={slide03} className="d-block w-100" alt="" />
          <div className="container">
            <div className="carousel-caption text-dark text-start">
              <h3>Men Collection 2018</h3>
              <h2>NEW ARRIVALS</h2>
              <button className="button btn btn-primary rounded-pill px-4">
                <a
                  href="HTML/shop.html"
                  className="text-white text-decoration-none"
                >
                  Shop Now
                </a>
              </button>
            </div>
          </div>
        </div>
      </div>
      <button
        style={{ left: "5%" }}
        type="button"
        className="text-muted fs-1 position-absolute w-auto carousel-control-prev"
        data-bs-target="#carouselSlider"
        data-bs-slide="prev"
      ></button>
      <button
        style={{ right: "5%" }}
        type="button"
        className="text-muted fs-1 position-absolute w-auto carousel-control-next"
        data-bs-target="#carouselSlider"
        data-bs-slide="next"
      ></button>
    </div>
  );
};

export default index;
