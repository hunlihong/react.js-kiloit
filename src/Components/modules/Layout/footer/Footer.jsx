import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";
import { FaFacebookF } from "react-icons/fa";
import { FaInstagram } from "react-icons/fa";
import { FaPinterestP } from "react-icons/fa";

const Footer = () => {
  return (
    <div className="footer d-block w-100 overflow-hidden pt-5 pb-3 bg-dark mt-5">
      <div className="container position-relative">
        <div className="row g-5 pb-5">
          <div className="col-lg-3 col-md-6 col-sm-12">
            <h6 className="fw-bold text-white mb-5">CATEGORIES</h6>
            <ul className="text-white list-unstyled lh-lg">
              <li>
                <a href="#">Women</a>
              </li>
              <li>
                <a href="#">Men</a>
              </li>
              <li>
                <a href="#">Shoes</a>
              </li>
              <li>
                <a href="#">Watches</a>
              </li>
            </ul>
          </div>
          <div className="col-lg-3 col-md-6 col-sm-12">
            <h6 className="fw-bold text-white mb-5">HELP</h6>
            <ul className="text-white list-unstyled lh-lg">
              <li>
                <a href="#">Track Order</a>
              </li>
              <li>
                <a href="#">Returns</a>
              </li>
              <li>
                <a href="#">Shipping</a>
              </li>
              <li>
                <a href="#">FAQs</a>
              </li>
            </ul>
          </div>
          <div className="col-lg-3 col-md-6 col-sm-12">
            <h6 className="fw-bold text-white mb-5">GET IN TOUCH</h6>
            <p className="text-white">
              Any questions? Let us know in store at 8th floor, 379 Hudson St,
              New York, NY 10018 or call us on (+1) 96 716 6789
            </p>
            <div className="medias d-flex gap-3 mt-5">
              <a className="fs-6" href="">
                <FaFacebookF />
              </a>
              <a className="fs-6" href="">
                <FaInstagram />
              </a>
              <a className="fs-6" href="">
                <FaPinterestP />
              </a>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 col-sm-12">
            <h6 className="fw-bold text-white mb-5">NEWSLETTER</h6>
            <input
              name="emailFooter"
              className="text-white border-0 border-bottom pb-2 mb-4 bg-transparent outline-0 d-block w-100"
              type="email"
              placeholder="email@example.com"
            />
            <button className="button footer-btn btn btn-primary rounded-pill">
              SUBSCRIBE
            </button>
          </div>
        </div>
        <div className="text-center">
          <p className="text-white">
            Copyright &copy;2024 All Rights Reserved Made with
            <i className="fa-regular fa-heart"></i> by{" "}
            <a href="" className="footer-link">
              Colorlib
            </a>{" "}
            and distributed by
            <a href="" className="footer-link">
              ThemeWagon
            </a>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Footer;
