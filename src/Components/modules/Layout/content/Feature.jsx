import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";
import item04 from "../../../../assets/item-cart-04.jpg";
import item05 from "../../../../assets/item-cart-05.jpg";

const Feature = () => {
  return (
    <div className="feature-container my-5">
      <div className="container">
        <ol className="breadcrumb mt-5">
          <li className="breadcrumb-item">
            <a href="../index.html">Home</a>
          </li>
          <li className="breadcrumb-item active text-muted">Feature</li>
        </ol>

        <div className="row mt-5 g-5">
          <div className="col-xl-8 col-12">
            <div className="table-container">
              <table className="table border text-center">
                <thead>
                  <tr>
                    <th className="fw-bold text-dark">PRODUCT</th>
                    <th className="fw-bold text-dark">PRICE</th>
                    <th className="fw-bold text-dark">QUANTITY</th>
                    <th className="fw-bold text-dark">TOTAL</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td className="py-5">
                      <div className="px-2 d-flex align-items-center gap-3">
                        <img src={item04} alt="" />
                        <p className="mb-0 fs-6">Fresh Strawberries</p>
                      </div>
                    </td>
                    <td className="h-100 position-relative">
                      <p className="mb-0 fs-6 position-absolute start-0 top-0 h-100 w-100 d-flex justify-content-center align-items-center">
                        $36.00
                      </p>
                    </td>
                    <td className="h-100 position-relative p-3">
                      <div className="position-absolute start-0 top-0 h-100 w-100 d-flex justify-content-center align-items-center">
                        <button className="minus-button px-3 py-1 rounded-start btn btn-secondary rounded-start-3 rounded-end-0">
                          -
                        </button>
                        <button
                          className="product-amount px-3 py-1 text-dark border-top border-bottom"
                          disabled
                        >
                          1
                        </button>
                        <button className="plus-button px-3 py-1 rounded-end btn btn-secondary rounded-end-3 rounded-start-0">
                          +
                        </button>
                      </div>
                    </td>
                    <td className="h-100 position-relative">
                      <p className="mb-0 fs-6 position-absolute start-0 top-0 h-100 w-100 d-flex justify-content-center align-items-center">
                        $36.00
                      </p>
                    </td>
                  </tr>
                  <tr>
                    <td className="py-5">
                      <div className="px-2 d-flex align-items-center gap-3">
                        <img src={item05} alt="" />
                        <p className="mb-0 fs-6">Lightweight Jacket</p>
                      </div>
                    </td>
                    <td className="h-100 position-relative">
                      <p className="mb-0 fs-6 position-absolute start-0 top-0 h-100 w-100 d-flex justify-content-center align-items-center">
                        $16.00
                      </p>
                    </td>
                    <td className="h-100 position-relative p-3">
                      <div className="position-absolute start-0 top-0 h-100 w-100 d-flex justify-content-center align-items-center">
                        {/* <button className="minus-button px-3 py-1 rounded-start">
                          -
                        </button>
                        <button
                          className="product-amount px-3 py-1 text-dark"
                          disabled
                        >
                          1
                        </button>
                        <button className="plus-button px-3 py-1 rounded-end">
                          +
                        </button> */}
                        <button className="minus-button px-3 py-1 rounded-start btn btn-secondary rounded-start-3 rounded-end-0">
                          -
                        </button>
                        <button
                          className="product-amount px-3 py-1 text-dark border-top border-bottom"
                          disabled
                        >
                          1
                        </button>
                        <button className="plus-button px-3 py-1 rounded-end btn btn-secondary rounded-end-3 rounded-start-0">
                          +
                        </button>
                      </div>
                    </td>
                    <td className="h-100 position-relative">
                      <p className="mb-0 fs-6 position-absolute start-0 top-0 h-100 w-100 d-flex justify-content-center align-items-center">
                        $16.00
                      </p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="mt-4 d-flex flex-column flex-md-row gap-4 justify-content-md-between flex-wrap">
              <div className="d-flex gap-3 flex-wrap">
                <input
                  name="couponCode"
                  className="btn btn-secondary rounded-pill px-4 button text-dark border border-dark bg-transparent fs-6"
                  type="text"
                  placeholder="Coupon Code"
                />
                <button className="btn btn-secondary rounded-pill px-4 button button-secondary fs-6">
                  APPLY COUPON
                </button>
              </div>
              <div>
                <button className="btn btn-secondary rounded-pill px-4 button button-secondary fs-6">
                  UPDATE CARD
                </button>
              </div>
            </div>
          </div>
          <div className="col-xl-4 col-12">
            <div className="p-4 border checkout">
              <div>
                <h5 className="fw-bold text-dark mb-4">CART TOTALS</h5>
                <div className="d-flex gap-5">
                  <p className="header">Subtotal</p>
                  <p className="header">$79.65</p>
                </div>
              </div>
              <div className="py-3">
                <div className="d-flex flex-column flex-sm-row gap-1 gap-sm-5">
                  <p className="header">Shipping</p>
                  <div>
                    <p className="check-text text-secondary">
                      There are no shipping methods available.Please check your
                      address, or contact us if you need any help.
                    </p>
                    <p className="text-uppercase text-start text-sm-center">
                      Calculate Shipping
                    </p>
                    <select
                      name="country"
                      className="d-block w-100 mb-2 p-2 text-secondary"
                    >
                      <option className="p-2" value="...">
                        Select a country...
                      </option>
                      <option className="p-2" value="usa">
                        USA
                      </option>
                      <option className="p-2" value="uk">
                        UK
                      </option>
                    </select>
                    <input
                      name="state"
                      className="d-block w-100 mb-2 p-2 text-secondary"
                      type="text"
                      placeholder="State / Country"
                    />
                    <input
                      name="postcard"
                      className="d-block w-100 mb-2 p-2 text-secondary"
                      type="text"
                      placeholder="Postcard / ZIP"
                    />
                    <button className="btn btn-secondary rounded-pill px-4 button button-secondary mt-2">
                      UPDATE TOTALS
                    </button>
                  </div>
                </div>
              </div>
              <div className="mt-3">
                <div className="d-flex gap-5">
                  <p className="header me-4">Total</p>
                  <p className="header">$79.65</p>
                </div>
                <button className="btn btn-dark rounded-pill px-4 py-2 button button-dark d-block w-100">
                  PROCEED TO CHECKOUT
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Feature;
