import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";
import bg from "../../../../assets/bg-01.jpg";
import about01 from "../../../../assets/about-01.jpg";
import about02 from "../../../../assets/about-02.jpg";

const About = () => {
  return (
    <div>
      <div
        style={{
          backgroundImage: `url(${bg})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          padding: "7rem 0",
        }}
        className="blog-header about-header"
      >
        <div className="container-fluid">
          <h2 className="text-white fs-1 fw-bold text-center">ABOUT</h2>
        </div>
      </div>
      <div className="about-container">
        <div className="container">
          <div className="row my-5 g-5">
            <div className="col-lg-8 col-md-12">
              <div className="text-muted me-lg-5 pe-lg-5">
                <h4 className="fw-bold text-dark mb-4">Our Story</h4>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Mauris consequat consequat enim, non auctor massa ultrices
                  non. Morbi sed odio massa. Quisque at vehicula tellus, sed
                  tincidunt augue. Orci varius natoque penatibus et magnis dis
                  parturient montes, nascetur ridiculus mus. Maecenas varius
                  egestas diam, eu sodales metus scelerisque congue.
                  Pellentesque habitant morbi tristique senectus et netus et
                  malesuada fames ac turpis egestas. Maecenas gravida justo eu
                  arcu egestas convallis. Nullam eu erat bibendum, tempus ipsum
                  eget, dictum enim. Donec non neque ut enim dapibus tincidunt
                  vitae nec augue. Suspendisse potenti. Proin ut est diam. Donec
                  condimentum euismod tortor, eget facilisis diam faucibus et.
                  Morbi a tempor elit.
                </p>
                <p>
                  Donec gravida lorem elit, quis condimentum ex semper sit amet.
                  Fusce eget ligula magna. Aliquam aliquam imperdiet sodales. Ut
                  fringilla turpis in vehicula vehicula. Pellentesque congue ac
                  orci ut gravida. Aliquam erat volutpat. Donec iaculis lectus a
                  arcu facilisis, eu sodales lectus sagittis. Etiam
                  pellentesque, magna vel dictum rutrum, neque justo eleifend
                  elit, vel tincidunt erat arcu ut sem. Sed rutrum, turpis ut
                  commodo efficitur, quam velit convallis ipsum, et maximus enim
                  ligula ac ligula.
                </p>
                <p>
                  Any questions? Let us know in store at 8th floor, 379 Hudson
                  St, New York, NY 10018 or call us on (+1) 96 716 6879
                </p>
              </div>
            </div>
            <div className="col-lg-4 col-md-12">
              <div className="position-relative hover-zoom">
                <div className="img-bounder overflow-hidden">
                  <img
                    className="d-block w-100 bg-image"
                    src={about01}
                    alt=""
                  />
                </div>
                <div className="d-none d-sm-block border border-secondary border-5 position-absolute h-100 w-100 end-0 top-0 m-4"></div>
              </div>
            </div>
          </div>
          <div className="row my-5 g-5">
            <div className="col-lg-8 col-md-12 order-lg-2">
              <div className="text-muted ms-lg-5 ps-lg-5">
                <h4 className="fw-bold text-dark mb-4">Our Mission</h4>
                <p>
                  Mauris non lacinia magna. Sed nec lobortis dolor. Vestibulum
                  rhoncus dignissim risus, sed consectetur erat. Pellentesque
                  habitant morbi tristique senectus et netus et malesuada fames
                  ac turpis egestas. Nullam maximus mauris sit amet odio
                  convallis, in pharetra magna gravida. Praesent sed nunc
                  fermentum mi molestie tempor. Morbi vitae viverra odio.
                  Pellentesque ac velit egestas, luctus arcu non, laoreet
                  mauris. Sed in ipsum tempor, consequat odio in, porttitor
                  ante. Ut mauris ligula, volutpat in sodales in, porta non
                  odio. Pellentesque tempor urna vitae mi vestibulum, nec
                  venenatis nulla lobortis. Proin at gravida ante. Mauris auctor
                  purus at lacus maximus euismod. Pellentesque vulputate massa
                  ut nisl hendrerit, eget elementum libero iaculis.
                </p>
                <blockquote className="ps-5 border-start border-3">
                  <p className="fst-italic">
                    Creativity is just connecting things. When you ask creative
                    people how they did something, they feel a little guilty
                    because they didn't really do it, they just saw something.
                    It seemed obvious to them after a while.
                  </p>
                  <span className="text-dark">- Steve Job's</span>
                </blockquote>
              </div>
            </div>
            <div className="col-lg-4 col-md-12 order-lg-1">
              <div className="position-relative hover-zoom">
                <div className="img-bounder overflow-hidden mb-5">
                  <img
                    className="d-block w-100 bg-image"
                    src={about02}
                    alt=""
                  />
                </div>
                <div className="d-none d-sm-block border border-secondary border-5 position-absolute h-100 w-100 start-0 top-0 m-4"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
