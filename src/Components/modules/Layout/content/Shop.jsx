import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";
import ProductList from "../../Home/ProductList";

const Shop = () => {
  return <ProductList />;
};

export default Shop;
