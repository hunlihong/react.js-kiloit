import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";
import blog1 from "../../../../assets/blog-01.jpg";
import blog2 from "../../../../assets/blog-02.jpg";
import blog3 from "../../../../assets/blog-03.jpg";
import min1 from "../../../../assets/product-min-01.jpg";
import min2 from "../../../../assets/product-min-02.jpg";
import min3 from "../../../../assets/product-min-03.jpg";
import bg from "../../../../assets/bg-02.jpg";
import { FaArrowRight } from "react-icons/fa6";

const Blog = () => {
  return (
    <div>
      <div
        style={{
          backgroundImage: `url(${bg})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          padding: "7rem 0",
        }}
        className="blog-header"
      >
        <div className="container-fluid">
          <h2 className="text-white fs-1 fw-bold text-center">BLOG</h2>
        </div>
      </div>
      <div className="blog-container my-5">
        <div className="container">
          <div className="row">
            <div className="col-md-8 col-sm-12">
              <div className="blog mb-5">
                <a href="blogDetail.html">
                  <div className="position-relative hover-zoom w-100 overflow-hidden">
                    <img
                      className="d-block w-100 bg-image"
                      src={blog1}
                      alt=""
                    />
                    <div className="p-2 text-center text-dark bg-light position-absolute top-0 start-0 m-3">
                      <h3 className="mb-0 fw-bold fs-4">22</h3>
                      <p className="mb-0 fs-6">Jan 2018</p>
                    </div>
                  </div>
                </a>
                <h3 className="title mb-0 fw-bold mt-3">
                  <a
                    href="blogDetail.html"
                    className="text-dark text-decoration-none"
                  >
                    8 Inspiring Ways to Wear Dresses in the Winter
                  </a>
                </h3>
                <p className="my-3">
                  Class aptent taciti sociosqu ad litora torquent per conubia
                  nostra, per inceptos himenaeos. Fusce eget dictum tortor.
                  Donec dictum vitae sapien eu varius
                </p>
                <div className="blog-poster d-flex flex-column flex-sm-row gap-sm-3 justify-content-between">
                  <p className="text-muted">
                    By <span className="text-dark">Admin</span> |{" "}
                    <span className="text-dark">
                      StreetStyle, Fashion, Couple
                    </span>{" "}
                    | <span className="text-dark">8 Comments</span>
                  </p>
                  <a
                    className="fw-bold text-dark text-decoration-none d-flex align-items-center gap-2"
                    href="blogDetail.html"
                  >
                    COUTINUE READING <FaArrowRight />
                  </a>
                </div>
              </div>
              <div className="blog mb-5">
                <a href="blogDetail.html">
                  <div className="position-relative hover-zoom w-100 overflow-hidden">
                    <img
                      className="d-block w-100 bg-image"
                      src={blog2}
                      alt=""
                    />
                    <div className="p-2 text-center text-dark bg-light position-absolute top-0 start-0 m-3">
                      <h3 className="mb-0 fw-bold fs-4">18</h3>
                      <p className="mb-0 fs-6">Jan 2018</p>
                    </div>
                  </div>
                </a>
                <h3 className="title mb-0 fw-bold mt-3">
                  <a
                    href="blogDetail.html"
                    className="text-dark text-decoration-none"
                  >
                    The Great Big List of Men's Gifts for the Holidays
                  </a>
                </h3>
                <p className="my-3">
                  Class aptent taciti sociosqu ad litora torquent per conubia
                  nostra, per inceptos himenaeos. Fusce eget dictum tortor.
                  Donec dictum vitae sapien eu varius
                </p>
                <div className="blog-poster d-flex flex-column flex-sm-row gap-sm-3 justify-content-between">
                  <p className="text-muted">
                    By <span className="text-dark">Admin</span> |{" "}
                    <span className="text-dark">
                      StreetStyle, Fashion, Couple
                    </span>{" "}
                    | <span className="text-dark">8 Comments</span>
                  </p>
                  <a
                    className="fw-bold text-dark text-decoration-none d-flex align-items-center gap-2"
                    href="blogDetail.html"
                  >
                    COUTINUE READING <FaArrowRight />
                  </a>
                </div>
              </div>
              <div className="blog mb-5">
                <a href="blogDetail.html">
                  <div className="position-relative hover-zoom w-100 overflow-hidden">
                    <img
                      className="d-block w-100 bg-image"
                      src={blog3}
                      alt=""
                    />
                    <div className="p-2 text-center text-dark bg-light position-absolute top-0 start-0 m-3">
                      <h3 className="mb-0 fw-bold fs-4">16</h3>
                      <p className="mb-0 fs-6">Jan 2018</p>
                    </div>
                  </div>
                </a>
                <h3 className="title mb-0 fw-bold mt-3">
                  <a
                    href="blogDetail.html"
                    className="text-dark text-decoration-none"
                  >
                    5 Winter-to-Spring Fashion Trends to Try Now
                  </a>
                </h3>
                <p className="my-3">
                  Class aptent taciti sociosqu ad litora torquent per conubia
                  nostra, per inceptos himenaeos. Fusce eget dictum tortor.
                  Donec dictum vitae sapien eu varius
                </p>
                <div className="blog-poster d-flex flex-column flex-sm-row gap-sm-3 justify-content-between">
                  <p className="text-muted">
                    By <span className="text-dark">Admin</span> |{" "}
                    <span className="text-dark">
                      StreetStyle, Fashion, Couple
                    </span>{" "}
                    | <span className="text-dark">8 Comments</span>
                  </p>
                  <a
                    className="fw-bold text-dark text-decoration-none d-flex align-items-center gap-2"
                    href="blogDetail.html"
                  >
                    COUTINUE READING <FaArrowRight />
                  </a>
                </div>
              </div>
              <div className="blog-btn my-5">
                <a
                  href="#"
                  className="btn btn-primary rounded-circle p-3 py-2 button button-secondary p-0 d-inline-block text-center fs-6 active me-2"
                >
                  1
                </a>
                <a
                  href="#"
                  className="btn btn-secondary rounded-circle p-3 py-2 button button-secondary p-0 d-inline-block text-center fs-6"
                >
                  2
                </a>
              </div>
            </div>
            <div className="col-md-4 col-sm-12">
              <div className="blog-features">
                <input
                  type="text"
                  className="btn btn-light rounded-pill px-4 text-start border-3 border-secondary button button-secondary fs-6 text-dark bg-transparent px-4 d-block w-100"
                  name="blogSearch"
                  placeholder="Search..."
                />
                <div>
                  <h4 className="fw-bold text-dark mt-5 mb-4">Categories</h4>
                  <ul className="list-unstyled">
                    <li className="border-top py-3">Fashion</li>
                    <li className="border-top py-3">Beauty</li>
                    <li className="border-top py-3">Street Style</li>
                    <li className="border-top py-3">Life Style</li>
                    <li className="border-top py-3 border-bottom">
                      DIY & Crafts
                    </li>
                  </ul>
                  <h4 className="fw-bold text-dark mt-5 mb-4">
                    Featured Products
                  </h4>
                  <div className="d-flex gap-3 align-items-center mb-3">
                    <img src={min1} alt="" />
                    <div>
                      <p>White Shirt with Pleat Detail Back</p>
                      <p>$19.00</p>
                    </div>
                  </div>
                  <div className="d-flex gap-3 align-items-center mb-3">
                    <img src={min2} alt="" />
                    <div>
                      <p>Converse All Star Hi Black Canvas</p>
                      <p>$39.00</p>
                    </div>
                  </div>
                  <div className="d-flex gap-3 align-items-center">
                    <img src={min3} alt="" />
                    <div>
                      <p>Nixon Porter Leather Watch in Tan</p>
                      <p>$17.00</p>
                    </div>
                  </div>
                </div>
                <div className="blog-achieve">
                  <h4 className="fw-bold text-dark mt-5 mb-4">Achieve</h4>
                  <div className="d-flex gap-3 justify-content-between text-muted">
                    <p>July 2018</p>
                    <p>(9)</p>
                  </div>
                  <div className="d-flex gap-3 justify-content-between text-muted">
                    <p>June 2018</p>
                    <p>(39)</p>
                  </div>
                  <div className="d-flex gap-3 justify-content-between text-muted">
                    <p>May 2018</p>
                    <p>(29)</p>
                  </div>
                  <div className="d-flex gap-3 justify-content-between text-muted">
                    <p>April 2018</p>
                    <p>(35)</p>
                  </div>
                  <div className="d-flex gap-3 justify-content-between text-muted">
                    <p>March 2018</p>
                    <p>(22)</p>
                  </div>
                  <div className="d-flex gap-3 justify-content-between text-muted">
                    <p>February 2018</p>
                    <p>(32)</p>
                  </div>
                  <div className="d-flex gap-3 justify-content-between text-muted">
                    <p>January 2018</p>
                    <p>(21)</p>
                  </div>
                  <div className="d-flex gap-3 justify-content-between text-muted">
                    <p>December 2017</p>
                    <p>(26)</p>
                  </div>
                </div>
                <div>
                  <h4 className="fw-bold text-dark mt-5 mb-4">Tags</h4>
                  <div className="blog-tags d-flex gap-2 flex-wrap">
                    <button className="btn btn-sm rounded-pill px-4 border bg-transparent button-sm">
                      Fashion
                    </button>
                    <button className="btn btn-sm rounded-pill px-4 border bg-transparent button-sm">
                      Lifestyle
                    </button>
                    <button className="btn btn-sm rounded-pill px-4 border bg-transparent button-sm">
                      Denim
                    </button>
                    <button className="btn btn-sm rounded-pill px-4 border bg-transparent button-sm">
                      Streetstyle
                    </button>
                    <button className="btn btn-sm rounded-pill px-4 border bg-transparent button-sm">
                      Crafts
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Blog;
