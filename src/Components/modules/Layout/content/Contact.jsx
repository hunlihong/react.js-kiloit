import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";
import bg from "../../../../assets/bg-01.jpg";
import { MdOutlineEmail } from "react-icons/md";
import { FaPhoneAlt } from "react-icons/fa";
import { IoLocationSharp } from "react-icons/io5";

const Contact = () => {
  return (
    <div>
      <div
        style={{
          backgroundImage: `url(${bg})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          padding: "7rem 0",
        }}
        className="blog-header about-header"
      >
        <div className="container-fluid">
          <h2 className="text-white fs-1 fw-bold text-center">CONTACT</h2>
        </div>
      </div>
      <div className="contact-container my-5">
        <div className="container">
          <div className="row g-0">
            <div className="col-md-6 col-sm-12">
              <div className="border border-secondary p-3 p-sm-5">
                <h4 className="text-center text-dark">Send Us A Message</h4>
                <div className="d-flex align-items-center gap-2 mt-3 py-1 px-3 border border-dark">
                  <span className="fs-4 m-0 p-0 d-flex align-items-center">
                    <MdOutlineEmail />
                  </span>
                  <input
                    style={{ outline: "none" }}
                    className="border-0 d-block w-100"
                    name="contactEmail"
                    type="email"
                    placeholder="Your Email Address"
                  />
                </div>
                <textarea
                  style={{ outline: "none" }}
                  className="border border-dark p-3 mt-3 d-block w-100"
                  name="contactText"
                  cols="30"
                  rows="10"
                  placeholder="How can we help?"
                ></textarea>
                <button className="btn btn-dark rounded-pill py-2 mt-3 button button-dark d-block w-100">
                  SUBMIT
                </button>
              </div>
            </div>
            <div className="col-md-6 col-sm-12">
              <div className="border border-secondary px-3 py-5 px-sm-5 h-100 d-flex flex-column justify-content-sm-center">
                <div className="d-flex gap-4">
                  <span className="fs-4">
                    <IoLocationSharp />
                  </span>
                  <div>
                    <p className="text-dark mb-0">Address</p>
                    <p className="text-muted">
                      Coza Store Center 8th floor, 379 Hudson St, New York, NY
                      10018 US
                    </p>
                  </div>
                </div>
                <div className="d-flex gap-4 my-5">
                  <span className="fs-5">
                    <FaPhoneAlt />
                  </span>
                  <div>
                    <p className="text-dark mb-0">Let's Talk</p>
                    <p className="phone">+1 800 1236879</p>
                  </div>
                </div>
                <div className="d-flex gap-4 align-items-start">
                  <span className="fs-4">
                    <MdOutlineEmail />
                  </span>
                  <div>
                    <p className="text-dark mb-0">Sale Support</p>
                    <p className="email">contact@example.com</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="mt-5">
        <iframe
          className="d-block w-100 border-0"
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4004.8875918223316!2d104.88385065613953!3d11.559222829632368!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x310951473174f105%3A0x3ef0a2faf0cf330b!2sKiloIT!5e0!3m2!1sen!2skh!4v1704620408408!5m2!1sen!2skh"
          height="450"
          allowFullScreen=""
          loading="lazy"
          referrerPolicy="no-referrer-when-downgrade"
        ></iframe>
      </div>
    </div>
  );
};

export default Contact;
