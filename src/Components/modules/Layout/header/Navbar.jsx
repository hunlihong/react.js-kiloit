import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";

const Navbar = () => {
  return (
    <nav className="navbar navbar-expand-lg shadow-0 py-2 w-100 border-bottom">
      <div className="container d-flex flex-wrap justify-content-between align-items-center w-100">
        <a href="#" className="navbar-brand fs-4 my-2">
          <span className="fw-bolder">COZA</span> STORE
        </a>
        <div className="d-flex justify-content-end align-items-center gap-3">
          <div className="d-flex d-lg-none gap-4 ps-2">
            <a
              href="#"
              className="fs-5 nav-icon"
              data-bs-toggle="modal"
              data-bs-target="#searchModal"
            >
              <i className="fa-solid fa-magnifying-glass"></i>
            </a>
            <a
              href="#"
              className="fs-5 nav-icon position-relative"
              data-bs-toggle="offcanvas"
              data-bs-target="#cartSidebar"
            >
              <div className="badge fw-normal rounded-pill bg-primary position-absolute top-0 start-100 translate-middle">
                2
              </div>
              <i className="fa-solid fa-cart-shopping"></i>
            </a>
            <a href="#" className="fs-5 nav-icon position-relative">
              <div className="badge fw-normal rounded-pill bg-primary position-absolute top-0 start-100 translate-middle">
                0
              </div>
              <i className="fa-regular fa-heart"></i>
            </a>
          </div>
          <span
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navList"
            aria-controls="navList"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <i className="navbar-toggler fa-solid fa-bars"></i>
          </span>
        </div>
        <div
          className="collapse navbar-collapse d-lg-flex justify-content-between align-items-center"
          id="navList"
        >
          <header>
            <div className="text-white d-flex flex-column d-lg-none justify-content-center align-items-start">
              <p className="mb-0 d-block w-100 p-2 border-bottom">
                Free shiping for standard order over $100
              </p>
              <ul className="mb-0 list-unstyled d-flex p-2">
                <li className="px-3 border-start">Help & FAQs</li>
                <li className="px-3 border-start">My Account</li>
                <li className="px-3 border-start">En</li>
                <li className="px-3 border-start border-end">USD</li>
              </ul>
            </div>
          </header>
          <ul className="navbar-nav gap-lg-3 ms-lg-5 px-2 px-lg-0">
            <li className="nav-item dropdown">
              <a
                role="button"
                href="/"
                className="nav-link Active"
                aria-expanded="false"
                id="dropdownMenuLink"
                activeClassName="active"
              >
                Home
              </a>
              {/* <ul
                className="dropdown-menu"
                id="homepageDropdown"
                aria-labelledby="dropdownMenuLink"
              >
                <li className="dropdown-item">
                  <a href="#">Homepage 1</a>
                </li>
                <li className="dropdown-item">
                  <a href="HTML/homepage2.html">Homepage 2</a>
                </li>
                <li className="dropdown-item">
                  <a href="HTML/homepage3.html">Homepage 3</a>
                </li>
              </ul> */}
            </li>
            <li className="nav-item">
              <a href="/shop" className="nav-link">
                Shop
              </a>
            </li>
            <li className="nav-item position-relative">
              <a href="/feature" className="nav-link">
                Features
                <div className="badge fw-normal bg-danger rounded-pill position-absolute top-0 start-lg-100 translate-middle">
                  HOT
                </div>
              </a>
            </li>
            <li className="nav-item">
              <a href="/blog" className="nav-link">
                Blog
              </a>
            </li>
            <li className="nav-item">
              <a href="/about" className="nav-link">
                About
              </a>
            </li>
            <li className="nav-item">
              <a href="/contact" className="nav-link">
                Contact
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
