import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";
import Navbar from "../Layout/header/Navbar";
import Footer from "../Layout/footer/Footer";
import Home from "../Home/Home";
import Shop from "./content/Shop";
import Feature from "./content/Feature";
import Blog from "./content/Blog";
import About from "./content/About";
import Contact from "./content/Contact";
import { BrowserRouter, Routes, Route } from "react-router-dom";

const Layout = () => {
  return (
    <div>
      <Navbar />
      <BrowserRouter>
        <Routes>
          <Route index path="/" element={<Home />} />
          <Route path="/shop" element={<Shop />} />
          <Route path="/feature" element={<Feature />} />
          <Route path="/blog" element={<Blog />} />
          <Route path="/about" element={<About />} />
          <Route path="/contact" element={<Contact />} />
        </Routes>
      </BrowserRouter>
      <Footer />
    </div>
  );
};

export default Layout;
