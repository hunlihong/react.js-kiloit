import React, { useReducer } from "react";

const initialState = {
  items: [],
  total: 0,
};

const reducer = (state, action) => {
  switch (action.type) {
    case "addItem":
      return {
        ...state,
        items: [...state.items, { id: action.id, count: 1 }],
        total: state.total + 1,
      };
    case "removeItem":
      const updatedItems = state.items.filter((item) => item.id !== action.id);
      return {
        ...state,
        items: updatedItems,
        total: state.total - 1,
      };
    case "incrementCount":
      return {
        ...state,
        items: state.items.map((item) =>
          item.id === action.id ? { ...item, count: item.count + 1 } : item
        ),
        total: state.total + 1,
      };
    case "decrementCount":
      return {
        ...state,
        items: state.items.map((item) =>
          item.id === action.id ? { ...item, count: item.count - 1 } : item
        ),
        total: state.total - 1,
      };
    default:
      return state;
  }
};

const ShoppingCart = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const handleAddItem = (id) => {
    dispatch({ type: "addItem", id });
  };

  const handleRemoveItem = (id) => {
    dispatch({ type: "removeItem", id });
  };

  const handleIncrementCount = (id) => {
    dispatch({ type: "incrementCount", id });
  };

  const handleDecrementCount = (id) => {
    dispatch({ type: "decrementCount", id });
  };

  return (
    <div>
      <h2>Shopping Cart</h2>
      <p>Total Items: {state.total}</p>
      <ul>
        {state.items.map((item) => (
          <li key={item.id}>
            Item {item.id} - Count: {item.count}
            <button onClick={() => handleIncrementCount(item.id)}>+</button>
            <button onClick={() => handleDecrementCount(item.id)}>-</button>
            <button onClick={() => handleRemoveItem(item.id)}>Remove</button>
          </li>
        ))}
      </ul>
      <button onClick={() => handleAddItem(Date.now())}>Add Item</button>
    </div>
  );
};

export default ShoppingCart;
