import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Modal from "react-bootstrap/Modal";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      record: {
        name: "",
        age: 0,
        gender: "Male",
        photo: "",
        programming: [],
        dob: "",
      },
      show: false,
      users: [],
    };
  }

  onChangeHandler = (e, key) => {
    if (key == "photo") {
      const src = URL.createObjectURL(e.target.files[0]);
      this.setState({
        record: { ...this.state.record, [key]: src },
      });
    } else if (key == "programming") {
      const { value, checked } = e.target;

      if (checked) {
        this.setState({
          record: {
            ...this.state.record,
            programming: [...this.state.record.programming, value],
          },
        });
      } else {
        const subProgramming = this.state.record.programming.filter(
          (p) => p !== value
        );
        this.setState({
          record: {
            ...this.state.record,
            programming: [subProgramming],
          },
        });
      }
    } else {
      this.setState({
        record: { ...this.state.record, [key]: e.target.value },
      });
    }
  };
  clearForm = () => {
    const emptyArray = [];
    this.setState({
      record: {
        name: "",
        age: 0,
        gender: "Male",
        photo: "",
        programming: emptyArray,
        dob: "",
      },
    });
  };
  onSaveHandler = () => {
    this.setState({ users: [...this.state.users, { ...this.state.record }] });
    this.setState({ show: false });
    this.clearForm();
  };

  render() {
    return (
      <div>
        <button
          onClick={() => this.setState({ show: true })}
          className="btn btn-primary float-end rounded-1 m-3"
        >
          Sign Up
        </button>
        <div className="container pt-3">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Age</th>
                <th>Gender</th>
                <th>Photo</th>
                <th>Programming</th>
                <th>Date of Birth</th>
              </tr>
            </thead>
            <tbody>
              {this.state.users.map(
                ({ name, age, gender, photo, programming, dob }, index) => {
                  return (
                    <tr key={index}>
                      <td>{index + 1}</td>
                      <td>{name}</td>
                      <td>{age}</td>
                      <td>{gender}</td>
                      <td>
                        <img
                          style={{
                            width: "100px",
                            height: "100px",
                            objectFit: "cover",
                          }}
                          src={photo}
                          alt=""
                        />
                      </td>
                      <td>
                        {programming.length == 2
                          ? programming.join(", ")
                          : programming}
                      </td>
                      <td>{dob}</td>
                    </tr>
                  );
                }
              )}
            </tbody>
          </table>
        </div>
        <Modal
          show={this.state.show}
          onHide={() => this.setState({ show: false })}
        >
          <Modal.Body>
            <form className="p-3 rounded-1">
              <h2 className="text-center text-dark mb-5">Register Form</h2>
              <label className="form-label mb-0" htmlFor="fullname">
                Enter full name
              </label>
              <input
                value={this.state.record.name}
                onChange={(e) => {
                  this.onChangeHandler(e, "name");
                }}
                size="sm"
                className="form-control mb-2"
                type="text"
                id="fullname"
              />
              <label className="form-label mb-0" htmlFor="age">
                Enter age
              </label>
              <input
                value={this.state.record.age}
                onChange={(e) => {
                  this.onChangeHandler(e, "age");
                }}
                size="sm"
                className="form-control mb-2"
                type="number"
                id="age"
              />
              <label className="form-label mb-0 me-4">
                Chooe gender
                <input
                  value="Male"
                  onChange={(e) => {
                    this.onChangeHandler(e, "gender");
                  }}
                  className="ms-3"
                  size="sm"
                  type="radio"
                  name="gender"
                  id="male"
                  defaultChecked
                />
                <label className="form-label mb-0 ms-2 me-3" htmlFor="male">
                  Male
                </label>
                <input
                  value="Female"
                  onChange={(e) => {
                    this.onChangeHandler(e, "gender");
                  }}
                  size="sm"
                  type="radio"
                  name="gender"
                  id="female"
                />
                <label
                  className="form-label mb-0 mb-2 ms-2 me-3"
                  htmlFor="female"
                >
                  Female
                </label>
              </label>
              <label className="d-block form-label" htmlFor="photo">
                Choose photo
              </label>
              <input
                size="sm"
                onChange={(e) => {
                  this.onChangeHandler(e, "photo");
                }}
                className="form-control mb-2"
                type="file"
                id="photo"
              />
              <label className="form-label">
                <p className="d-block mb-1">Choose programming language(s)</p>
                <input
                  value="JavaScript"
                  onChange={(e) => this.onChangeHandler(e, "programming")}
                  size="sm"
                  name="programming"
                  className="mb-2"
                  type="checkbox"
                  id="js"
                />
                <label className="form-label mb-0 ms-2 me-3" htmlFor="js">
                  JavaScript
                </label>
                <input
                  value="React.JS"
                  onChange={(e) => this.onChangeHandler(e, "programming")}
                  size="sm"
                  name="programming"
                  className="mb-2"
                  type="checkbox"
                  id="react"
                />
                <label className="form-label mb-0 ms-2 me-3" htmlFor="react">
                  React.JS
                </label>
              </label>
              <label className="form-label mb-0 d-block" htmlFor="date">
                Date of birth
              </label>
              <input
                value={this.state.record.dob}
                onChange={(e) => {
                  this.onChangeHandler(e, "dob");
                }}
                size="sm"
                className="form-control mb-3"
                type="date"
                id="date"
              />
            </form>
          </Modal.Body>
          <Modal.Footer>
            <button
              onClick={() => this.setState({ show: false })}
              className="btn btn-secondary rounded-1"
            >
              Cancel
            </button>
            <input
              onClick={this.onSaveHandler}
              type="submit"
              value="Submit"
              className="btn btn-primary rounded-1"
            />
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default Form;
