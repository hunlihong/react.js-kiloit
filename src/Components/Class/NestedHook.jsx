import React, { useState } from "react";

const TestingHook = () => {
  const [state, setState] = useState({
    form: {
      name: "",
      major: "",
    },
  });
  return (
    <div>
      <form action="">
        <input
          value={state.form.name}
          onChange={(e) => {
            setState({
              ...state,
              form: { ...state.form, name: e.target.value },
            });
          }}
          type="text"
        />
      </form>
      <p>{state.form.name}</p>
    </div>
  );
};

export default TestingHook;
