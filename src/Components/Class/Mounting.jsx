import React from "react";

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = { color: "red" };
  }

  static getDerivedStateFromProps(props, state) {
    return { color: props.modifycolor };
  }

  render() {
    return (
      <div>
        <h1>{this.state.color}</h1>
      </div>
    );
  }
}

export default Header;
