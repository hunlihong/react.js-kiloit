import React from "react";
import Item from "./Item";

const names = ["Lihong", "Vannith", "Chris", "Speed"];
const List = () => {
  return (
    <div>
      {names.map((name, index) => {
        return <Item key={index} name={name} />;
      })}
    </div>
  );
};

export default List;
