import React from "react";

class Updating extends React.Component {
  constructor(props) {
    super(props);
    this.state = { color: "yellow" };
  }

  shouldComponentUpdate() {
    return false;
  }

  clickHandler = () => {
    this.setState({ color: "red" });
  };

  render() {
    return (
      <div>
        <h1>{this.state.color}</h1>
        <button onClick={this.clickHandler}>Click here to change color</button>
      </div>
    );
  }
}

export default Updating;
