import { useReducer } from "react";
import "bootstrap/dist/css/bootstrap.min.css";

const initialState = {
  items: [],
  amount: 0,
  checked: false,
};

const reducer = (state, action) => {
  switch (action.type) {
    case "add":
      console.log("add button is clicked");
    case "remove":
      console.log("remove button is clicked");
  }
};

const Todo = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  console.log(state, dispatch);

  function handleAdd() {
    dispatch({ type: "add" });
  }
  function handleRemove() {
    dispatch({ type: "remove" });
  }

  return (
    <div className="m-5">
      <h2>Todo List</h2>
      <button onClick={handleAdd} className="btn btn-primary">
        Add
      </button>
      <button onClick={handleRemove} className="btn btn-danger ms-3">
        Remove
      </button>
    </div>
  );
};

export default Todo;
