import React from "react";

class Car extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <h1>This car is {this.props.model}</h1>
      </div>
    );
  }
}

export default Car;
