import React from "react";

class Unmounting extends React.Component {
  constructor(props) {
    super(props);
    this.state = { show: true };
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    document.querySelector(
      ".sms"
    ).innerHTML = `The prev value of show is ${prevState.show}`;
  }

  toggleHandler = () => {
    this.setState({ show: !this.state.show });
  };

  render() {
    return (
      <div>
        <p className="sms"></p>
        <div>{this.state.show ? <Message /> : null}</div>
        <button onClick={this.toggleHandler}>Toggle here</button>
      </div>
    );
  }
}

class Message extends React.Component {
  componentWillUnmount() {
    alert("The message is gonna unmounted from DOM.");
  }

  render() {
    return <h1>Welcome to KiloIT</h1>;
  }
}

export default Unmounting;
