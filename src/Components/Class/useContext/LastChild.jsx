import { useContext } from "react";
import { UserContext } from "./Parent";

const LastChild = () => {
  const { user, updateUser } = useContext(UserContext);

  return (
    <div>
      <h1>Hello {user}</h1>
      <button onClick={() => updateUser("Lihong is cooking on useContext")}>
        Let the machine cook
      </button>
    </div>
  );
};

export default LastChild;
