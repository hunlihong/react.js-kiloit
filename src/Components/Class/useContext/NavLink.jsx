import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import User from "./User";

const NavLink = () => {
  return (
    <div className="d-flex justify-content-between align-items-center w-100">
      <ul className="navbar-nav">
        <li className="nav-link">
          <a href="#" className="nav-item">
            Home
          </a>
        </li>
        <li className="nav-link">
          <a href="#" className="nav-item">
            About
          </a>
        </li>
        <li className="nav-link">
          <a href="#" className="nav-item">
            Products
          </a>
        </li>
        <li className="nav-link">
          <a href="#" className="nav-item">
            Contact
          </a>
        </li>
      </ul>
      <User />
    </div>
  );
};

export default NavLink;
