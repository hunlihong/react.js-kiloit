import "bootstrap/dist/css/bootstrap.min.css";
import NavLink from "./NavLink";

const NavBar = () => {
  return (
    <div className="navbar navbar-expand-lg">
      <div className="container">
        <a href="#" className="navbar-brand">
          <img
            style={{ width: "50px" }}
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Facebook_Logo_%282019%29.png/480px-Facebook_Logo_%282019%29.png"
            alt=""
          />
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navList"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse">
          <NavLink />
        </div>
      </div>
    </div>
  );
};

export default NavBar;
