import { createContext, useState } from "react";
import LastChild from "./LastChild";

export const UserContext = createContext();

const Parent = () => {
  const [user, setUser] = useState("");
  const updateUser = (newUser) => setUser(newUser);
  return (
    <UserContext.Provider value={{ user, updateUser }}>
      <input
        value={user}
        onChange={(e) => setUser(e.target.value)}
        type="text"
        placeholder="Enter your name..."
      />
      <LastChild />
    </UserContext.Provider>
  );
};

export default Parent;
