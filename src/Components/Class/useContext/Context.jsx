import { useState, createContext, useContext } from "react";

const GlobalContext = createContext();
export const useGlobalContext = () => useContext(GlobalContext);
const Context = (props) => {
  const [user, setUser] = useState("Lihong");
  function signOut() {
    setUser(null);
  }
  return (
    <GlobalContext.Provider value={{ user, signOut }}>
      {props.children}
    </GlobalContext.Provider>
  );
};

export default Context;
