import React from "react";

class Person extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: "Testing" };
  }
  changeName = () => {
    this.setState({ name: "Motherfucker" });
  };

  render() {
    return (
      <div>
        <h1>{this.state.name}</h1>
        <button onClick={this.changeName}>Click me</button>
      </div>
    );
  }
}

export default Person;
