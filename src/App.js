import Layout from "./Components/Class/UseReducer";
import "./Components/modules/style.css";

const App = () => {
  return (
    <div>
      <Layout />
    </div>
  );
};

export default App;
